/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.midterm_algorithm;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class Midterm_01 {

    public static void main(String[] args) {
        Scanner wn = new Scanner(System.in);
        int[] arr = new int[7];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = wn.nextInt();
        }

        System.out.println(MaxsubFaster(arr));

    }

    public static int MaxsubFaster(int[] arr) {
        int sum[] = new int[arr.length];
        sum[0] = 0;

        for (int i = 1; i < arr.length; i++) {
            sum[i] = sum[i - 1] + arr[i];
        }

        int max = 0;

        for (int j = 1; j < arr.length; j++) {
            for (int k = j; k < arr.length; k++) {
                int sumTotal = sum[k] - sum[j - 1];

                if (sumTotal > max) {
                    max = sumTotal;
                }

            }
        }

        return max;
    }
}
